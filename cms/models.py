from django.db import models

# Create your models here.
class Contenido(models.Model):
    clave = models.CharField(max_length=64)
    valor = models.TextField()

    def __str__(self):
        return "ID=" + str(self.id) + " --- " + self.clave + "," + self.valor

    def contiene_pagina(self):
        return ('Pagina' in self.valor)

class Comentario(models.Model):
    contenido = models.ForeignKey(Contenido, on_delete =  models.CASCADE)
    titulo = models.CharField(max_length=128)
    cuerpo = models.TextField()
    fecha = models.DateTimeField()

    def __str__(self):
        return "ID=" + str(self.id) + " --- " + self.titulo + "," + str(self.fecha) + "," + self.cuerpo + "," + str(self.contenido.id)
