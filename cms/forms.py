from django import forms
from .models import Contenido, Comentario

class ContentForm(forms.ModelForm):
    class Meta:
        model = Contenido
        fields = ('clave', 'valor')

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ('titulo', 'cuerpo')
