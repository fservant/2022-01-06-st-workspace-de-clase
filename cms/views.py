from django.contrib.auth import logout
from django.shortcuts import render, redirect

# Create your views here.
from django.http import HttpResponse, HttpRequest
from .models import Contenido, Comentario
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from .forms import ContentForm, CommentForm

class Counter():

    def __init__(self):
        self.count: int = 0

    def increment(self) -> int:
        self.count += 1
        return(self.count)

counter: Counter = Counter()

@csrf_exempt
def get_content(request, llave):

    if (request.method == "POST"):
        action = request.POST['action']
        if action == "Modificar contenido":
            value = request.POST['valor']
            try:
                contenido = Contenido.objects.get(clave=llave)
                contenido.valor = value
            except Contenido.DoesNotExist:
                contenido = Contenido(clave=llave, valor=value)
            contenido.save()
        elif action == "Anadir comentario":
            contenido = Contenido.objects.get(clave=llave)
            titulo = request.POST['titulo']
            cuerpo = request.POST['cuerpo']
            fecha = timezone.now()
            q = Comentario(contenido=contenido, titulo=titulo, cuerpo=cuerpo, fecha=fecha)
            q.save()
    try:
        contenido = Contenido.objects.get(clave=llave)
        context = {"contenido": contenido}
        return render(request, 'cms/content.html', context)

    except Contenido.DoesNotExist:
        context = {"llave": llave}
        return render(request, 'cms/new.html', context)


def cms_new(request, llave):
    if request.method == "POST":
        form = ContentForm(request.POST)
        if form.is_valid():
            valor = form.save()
            return redirect('mostrar contenido', llave=llave)
    form = ContentForm()
    return render(request, 'cms/add.html', {'form': form, 'is_content': True})

def comment_new(request, llave):
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            content = Contenido.objects.get(clave=llave)
            comment = Comentario(contenido = content, titulo = request.POST['titulo'], cuerpo = request.POST['cuerpo'], fecha = timezone.now())
            comment.save()
            return redirect('mostrar contenido', llave=llave)
    form = CommentForm()
    return render(request, 'cms/add.html', {'form': form, 'is_content': False})

def index(request):
    content_list = Contenido.objects.all()
    template = loader.get_template('cms/index.html')
    context = {
        'content_list': content_list
    }
    return HttpResponse(template.render(context, request))

def loggedIn(request):
    if request.user.is_authenticated:
        respuesta = "Logueado como " + request.user.username
    else:
        respuesta = "No estas logueado :( Puedes hacerlo aqui: <a href='/login'>Loguéame</a>"

    return HttpResponse(respuesta)

def logout_view(request):
    logout(request)
    return redirect("/cms/")

def imagen(request):
    template = loader.get_template('cms/plantilla.html')
    context = {}
    return HttpResponse(template.render(context, request))
